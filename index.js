const Discord = require('discord.js');
const dotenv = require('dotenv');
const path = require('path');

const { FLAGS, HELP } = require('./data/constants');
const { getRandomFromList } = require('./util');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({ path: path.join(__dirname, '.local.env') });
}

const {
  scrapeAllFandoms, scrapeChainedPages, scrapePage, scrapeRandomPages,
} = require('./scrape');
const {
  cli: fromCLI,
  display: fromDisplay,
  fandom: fromFandom,
  option: fromOption,
  user: fromUser,
  work: fromWork,
} = require('./commands');

const db = require('./database/db');

const client = new Discord.Client();

const internals = {};

// config
const token = process.env.DISCORD_TOKEN;
const prefix = '!';

const refreshHistory = async () => {
  // transform guild map to array
  const guilds = Array.from(client.guilds.values());
  // loop through all available guilds
  await guilds.forEach(async (guild) => {
    // transform channels map to array
    const channels = Array.from(guild.channels.values());
    // loop through all channels for this guild
    channels
      .filter((channel) => {
        // ignore non-text channels
        if (channel.type !== 'text') { return false; }

        // ignore channels the bot doesn't have permissions for
        const permissions = channel.permissionsFor(guild.me);
        return permissions.has('READ_MESSAGES') &&
          permissions.has('READ_MESSAGE_HISTORY') &&
          permissions.has('SEND_MESSAGES');
      })
      .forEach(async (channel) => {
        // fetch the last 100 messages for this channel
        await channel.fetchMessages({ limit: 100 });
      });
  });
  client.user.setActivity(`recs on ${client.guilds.size} server${client.guilds.size === 1 ? '' : 's'}`);
};

client.on('ready', () => {
  /* eslint-disable-next-line no-console */
  console.log(`
    Bot has started, with ${client.users.size} users, 
    in ${client.channels.size} channels of ${client.guilds.size} guilds.
  `);

  client.user.setActivity('Server update and refreshing channels . . .');
  refreshHistory();
});

client.on('messageReactionAdd', async (messageReaction, user) => {
  if (user.bot) { return; }

  if (messageReaction.emoji.identifier === '%F0%9F%91%81') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.markRead(id, url);
  }

  if (messageReaction.emoji.identifier === '%E2%AD%90') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.markFav(id, url);
  }

  if (messageReaction.emoji.identifier === '%F0%9F%94%96') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.markLater(id, url);
  }
});

client.on('messageReactionRemove', async (messageReaction, user) => {
  if (user.bot) { return; }

  if (messageReaction.emoji.identifier === '%F0%9F%91%81') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.unmarkRead(id, url);
  }

  if (messageReaction.emoji.identifier === '%E2%AD%90') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.unmarkFav(id, url);
  }

  if (messageReaction.emoji.identifier === '%F0%9F%94%96') {
    const [{ url }] = messageReaction.message.embeds;
    const { id } = user;
    fromWork.unmarkLater(id, url);
  }
});

client.on('message', async (message) => {
  if (message.author.bot) { return; }

  if (message.content.indexOf(prefix) !== 0) { return; }

  const args = message.content.slice(prefix.length).trim().split(/ +|\n+/g);
  const command = args.shift().toLowerCase();
  const flags = args.filter(x => x.startsWith('-'));

  /* FANDOMS */
  if (command === 'fandoms') {
    if (flags.length && flags.includes('-r')) {
      fromFandom.getRandomFandoms().then(fs => fromDisplay.showRandomFandoms(fs, message));
    } else {
      fromFandom.getFandomsList(args, message, 'new')
        .then(list => fromDisplay.showFandomsList(list, message))
        .then(newMessage => fromUser.updateMessageId(message, newMessage.id));
    }
  }

  if (command === 'name') {
    const [oldId, newId] = args;
    fromUser.isUserAdmin(message.author.id)
      .then((isAdmin) => {
        if (!isAdmin) {
          fromDisplay.adminOnlyMessage(message);
        } else {
          fromFandom.updateId(oldId, newId)
            .then(([affected]) => {
              if (affected) {
                message.channel.send(`Updated \`${oldId}\` to \`${newId}\``);
              } else {
                message.channel.send('Not updated. Duplicated or invalid value, or something went wrong.');
              }
            });
        }
      });
  }

  /* FAVORITES */
  if (command === 'favs') {
    fromUser.getFavoritesList(args, message, 'new')
      .then(list => fromDisplay.showFavoritesList(list, message))
      .then(newMessage => fromUser.updateMessageId(message, newMessage.id));
  }

  if (command === 'fav') {
    fromUser.getFavoritesFromUserId(message.author.id, args.join(' '))
      .then(list => getRandomFromList(list) || { empty: true })
      .then(work => fromDisplay.showRec(work, message));
  }

  if (command === 'addfav') {
    const { id } = message.author;
    fromWork.findUrls(args)
      .then((found) => {
        found.works.forEach((url) => {
          fromWork.markFav(id, url);
        });
        return found;
      })
      .then(found => fromDisplay.acknowledgeFav(found, message));
  }

  /* READ LATERS */
  if (command === 'laters') {
    fromUser.getLatersList(args, message, 'new')
      .then(list => fromDisplay.showLatersList(list, message))
      .then(newMessage => fromUser.updateMessageId(message, newMessage.id));
  }

  if (command === 'later') {
    fromUser.getLatersFromUserId(message.author.id, args.join(' '))
      .then(list => getRandomFromList(list) || { empty: true })
      .then(work => fromDisplay.showRec(work, message));
  }

  if (command === 'addlater') {
    const { id } = message.author;
    fromWork.findUrls(args)
      .then((found) => {
        found.works.forEach((url) => {
          fromWork.markLater(id, url);
        });
        return found;
      })
      .then(found => fromDisplay.acknowledgeLater(found, message));
  }

  /* PAGINATION */
  if (['next', 'prev'].includes(command)) {
    fromUser.getChannelMessage(message)
      .then((saved) => {
        if (!saved) { return null; }
        switch (saved.type) {
          case 'fandoms':
            return fromFandom.getFandomsList(args, message, command)
              .then(list => fromDisplay.showFandomsList(list, message));
          case 'favorites':
            return fromUser.getFavoritesList(args, message, command)
              .then(list => fromDisplay.showFavoritesList(list, message));
          case 'laters':
            return fromUser.getLatersList(args, message, command)
              .then(list => fromDisplay.showLatersList(list, message));
          default:
            return null;
        }
      });
  }

  if (command === 'show') {
    fromUser.getFicFromList(message, args[0])
      .then(work => fromDisplay.showRec(work, message));
  }

  if (command === 'remove') {
    fromUser.getFicFromList(message, args[0])
      .then(fic => fromUser.removeFromList(message, fic)
        .then(where => message.channel.send(`Successfully removed ${fic.title} from ${where}`))
        .catch(error => message.channel.send(error)));
  }

  /* RANDOM WORK */
  if (command === 'rec' || command === 'random') {
    const rawFandoms = args.filter(x => !x.startsWith('-'));
    internals.handleRec(command, message, flags, rawFandoms);
  }

  /* REPEAT SEARCH */
  if (command === 'rep') {
    fromUser.getSearchOptions(message.author)
      .then(({ fandoms: rawFandoms, flags: savedFlags, type }) => {
        internals.handleRec(type, message, savedFlags, rawFandoms);
      });
  }

  /* MESSAGE HISTORY REFRESH */
  if (command === 'refresh') {
    refreshHistory();
    message.channel.send('Refreshed all messages');
  }

  /* SCRAPE */
  if (command === 'singlescrape') {
    const [url] = args;
    fromUser.isUserAdmin(message.author.id)
      .then((isAdmin) => {
        if (!isAdmin) {
          fromDisplay.adminOnlyMessage(message);
        } else {
          fromFandom.addOrUpdateFandom(url)
            .then(fandom => fromWork.findMedians(fandom)
              .then(medians => scrapePage(url, message, fandom, medians)));
        }
      });
  }

  if (command === 'scrape') {
    const [url] = args;

    fromUser.isUserAdmin(message.author.id)
      .then((isAdmin) => {
        if (!isAdmin) {
          fromDisplay.adminOnlyMessage(message);
        } else {
          if (url === 'all') { // eslint-disable-line no-lonely-if
            fromFandom.getAll().then(fandoms => scrapeAllFandoms(message, fandoms));
          } else {
            fromFandom.addOrUpdateFandom(url)
              .then(fandom => fromWork.findMedians(fandom)
                .then(medians => scrapeChainedPages(url, message, fandom, medians)));
          }
        }
      });
  }

  /* HELP */
  if (command === 'help') {
    message.channel.send({
      embed: new Discord.RichEmbed()
        .setTitle('Help')
        .setDescription(HELP.DESCRIPTION)
        .addField('Getting Started', HELP.START)
        .addField('⠀', '⠀')
        .addField('Getting Recommendations', HELP.RECS, true)
        .addField('Fandoms', HELP.FANDOMS, true)
        .addField('⠀', '⠀')
        .addField('Search Flags', HELP.FLAGS, true)
        .addField('Reactions', HELP.REACTS, true)
        .addField('⠀', '⠀')
        .addField('Favorites', HELP.FAVS, true)
        .addField('Read Laters', HELP.LATERS, true)
        .setFooter('Remember, you can DM the bot for a more private experience!'),
    });
  }

  //   // // // // // // //
  //  // ADMIN SECTION *//
  // // // // // // // //

  /* TRACKING CHANNEL */
  if (command === 'trackhere') {
    fromUser.isUserAdmin(message.author.id)
      .then((isAdmin) => {
        if (!isAdmin) {
          fromDisplay.adminOnlyMessage(message);
        } else {
          fromOption.setTrackingChannel(message)
            .then(() => message.channel.send('Tracking channel set.'));
        }
      });
  }
});

process.stdin.setEncoding('utf8');
process.stdin.on('data', (args) => {
  const [command, id] = args.slice(prefix.length).trim().split(/ +|\n+/g);

  if (command === 'scrape') {
    fromUser.isUserAdmin(id)
      .then((isAdmin) => {
        if (isAdmin) {
          fromFandom.getAll().then(fandoms => fromCLI.scrapeAllFandoms(client, fandoms));
        }
      });
  }
});

db.sync().then(() => {
  client.login(token);
});

internals.handleRec = (command, message, flags, rawFandoms) => {
  if (!rawFandoms || !rawFandoms.length) {
    fromWork.findRandomWork(command, message, { fandoms: [], flags })
      .then(work => fromDisplay.showRec(work, message));
  } else if (flags.includes(FLAGS.OPTIONS.FRESH)) {
    fromWork.getFandomsFromRec(rawFandoms)
      .then((fandoms) => {
        // do a background scrape and rely on scrape results
        const fandom = getRandomFromList(fandoms);
        return scrapeRandomPages(fandom, 3).then(({ onTimeout, urls }) => {
          if (onTimeout) {
            return { fandoms, urls: [] };
          }
          return { fandoms, urls };
        });
      })
      .then(({ fandoms, urls }) => fromWork.findRandomWork(command, message, { fandoms, flags, urls })) // eslint-disable-line max-len
      .then(work => fromDisplay.showRec(work, message));
  } else {
    fromWork.getFandomsFromRec(rawFandoms)
      .then((fandoms) => {
        // do a three-page background scrape
        // do not rely on scrape results
        const fandom = getRandomFromList(fandoms);
        scrapeRandomPages(fandom, 3);
        return fandoms;
      })
      .then(fandoms => fromWork.findRandomWork(command, message, { fandoms, flags }))
      .then(work => fromDisplay.showRec(work, message));
  }
};

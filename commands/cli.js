const { scrapeChainedPages } = require('../scrape');
const { findMedians } = require('../commands/work');

const scrapeAllFandoms = (client, fandoms = [], medians = {}) => {
  if (!fandoms.length) return null;
  const [fandom] = fandoms;
  const url = `https://${fandom.url}`;

  const fakeMessage = { client };

  return findMedians(fandom)
    .then(scrapeChainedPages(url, fakeMessage, fandom, medians, false))
    .then(() => {
      const newFandomsList = fandoms.slice(1);
      if (!newFandomsList.length) { return null; }
      return scrapeAllFandoms(client, newFandomsList, medians);
    });
};

module.exports = {
  scrapeAllFandoms,
};

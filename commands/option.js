const Option = require('../database/option');

const { OPTION_TYPES } = require('../data/constants');

const setTrackingChannel = message => Option.addOrUpdateOption({
  type: OPTION_TYPES.TRACKING_CHANNEL,
  value: message.channel.id,
  adminOnly: true,
});

module.exports = {
  setTrackingChannel,
};

const Url = require('url');

const Work = require('../database/work');
const Fandom = require('../database/fandom');
const User = require('../database/user');
const { FLAGS } = require('../data/constants');
const { getPureFicUrl } = require('../util');

const getRawFandoms = fandoms => fandoms.reduce((memo, fandom) => {
  // eslint-disable-next-line no-useless-escape
  if (/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(fandom)) {
    const parser = Url.parse(fandom);
    const url = `${parser.host}${parser.pathname}`;
    return {
      ...memo,
      urls: [...memo.urls, url],
    };
  }
  return {
    ...memo,
    ids: [...memo.ids, fandom],
  };
}, { ids: [], urls: [] });

const pickByScore = works => works.reduce((memo, work) => {
  if (work.score > memo.score) {
    return work;
  }
  return memo;
}, { score: -1, empty: true });

const getFandomsFromRec = (fandoms) => {
  const rawFandoms = getRawFandoms(fandoms);
  return Fandom.getFandomsFromLists(rawFandoms);
};

const getFieldMedian = async (fandomId, field) => Work.getTopByField(fandomId, field, 20)
  .then(works => works.reduce((count, work) => count + work[field], 0) / (works.length || 1));

const findMedians = async fandom => getFieldMedian(fandom.id, 'kudos')
  .then(kudos => getFieldMedian(fandom.id, 'bookmarks')
    .then(bookmarks => ({ kudos, bookmarks })));

const findRandomWork = (type, message, { fandoms, flags, urls }) => {
  const fandomIds = fandoms.map(fandom => fandom.id);
  User.setOptions(message.author, { flags, type, fandoms: fandomIds });

  const ratings = flags
    .filter(f => Object.keys(FLAGS.RATING).includes(f))
    .map(f => FLAGS.RATING[f]);
  const complete = flags.includes('-c');

  const options = { complete, fandoms: fandomIds, ratings };

  if (type === 'rec') {
    return Work.getRandomUnreadByUser(message.author.id, options, urls)
      .then((works) => {
        if (works.length < 50) {
          const limit = 50 - works.length;
          return Work.getRandomUnreadByUser(message.author.id, { ...options, limit, urls });
        }
        return works;
      })
      .then(pickByScore);
  }

  return Work.getRandom(options, urls)
    .then((works) => {
      if (works.length < 50) {
        const limit = 50 - works.length;
        return Work.getRandom({ ...options, limit, urls });
      }
      return works;
    })
    .then(pickByScore);
};

const findUrls = (urls, possMemo) => {
  const memo = possMemo || {
    invalidUrls: [],
    missingWorks: [],
    works: [],
  };

  const url = urls.pop();
  const pureUrl = getPureFicUrl(url);

  if (!pureUrl) {
    if (!memo.invalidUrls.includes(url)) {
      memo.invalidUrls.push(url);
    }

    if (urls.length) {
      return findUrls(urls, memo);
    }

    return new Promise((resolve) => {
      resolve(memo);
    });
  }

  return Work.findByPk(pureUrl)
    .then((work) => {
      if (!work && !memo.missingWorks.includes(pureUrl)) {
        memo.missingWorks.push(pureUrl);
      } else if (!memo.works.includes(work.url)) {
        memo.works.push(work.url);
      }

      if (urls.length) {
        return findUrls(urls, memo);
      }

      return memo;
    });
};

const repeatSearch = message => User.getSearchOptions(message.author)
  .then((options) => {
    const { fandoms, flags, type } = options;
    return getFandomsFromRec(fandoms)
      .then(fandomInsts => findRandomWork(type, message, { fandoms: fandomInsts, flags }));
  });

const getUserFavorites = (message) => {
  const { id } = message.author;
  return User
    .findByPk(id)
    .then(user => user
      .getFavorite()
      .then(favorites => ({ favorites, user })));
};

const markFav = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.addFavorite(work);
        });
    });
};

const markRead = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.addRead(work);
        });
    });
};

const markLater = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.addLater(work);
        });
    });
};

const unmarkFav = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.removeFavorite(work);
        });
    });
};


const unmarkRead = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.removeRead(work);
        });
    });
};

const unmarkLater = (userId, url) => {
  Work
    .findByUrl(url)
    .then((work) => {
      User
        .findByPk(userId)
        .then((user) => {
          user.removeLater(work);
        });
    });
};

module.exports = {
  findMedians,
  findRandomWork,
  findUrls,
  getFandomsFromRec,
  getUserFavorites,
  markFav,
  markRead,
  markLater,
  unmarkFav,
  unmarkRead,
  unmarkLater,
  repeatSearch,
};

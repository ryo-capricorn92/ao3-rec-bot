const Discord = require('discord.js');

const { pairingNames, shortNames } = require('../data/characters');
const { RATING_COLOR, TEMP_AVI } = require('../data/constants');
const { pairingUrls } = require('../data/images');

const { truncate } = require('../util');

const sendMessagesInOrder = (channel, messages) => {
  const message = messages.shift();
  return channel.send(message)
    .then(() => {
      if (messages.length) {
        return sendMessagesInOrder(channel, messages);
      }
      return null;
    });
};

const acknowledgeFav = (found, message) => {
  const alerts = [];

  if (found.works.length) {
    alerts.push(`Success! Added ${found.works.length} work${found.works.length === 1 ? '' : 's'} to your favorites`);
  }

  if (found.missingWorks.length) {
    alerts.push('Works must be in the fandom database to be added. The following URLs were not added to your favorites:');
    found.missingWorks.forEach((url) => {
      alerts.push(url);
    });
  }

  if (found.invalidUrls.length) {
    alerts.push('Please use valid work URLs. The following URLs are not valid AO3 fic URLs:');
    found.invalidUrls.forEach((url) => {
      alerts.push(url);
    });
  }

  sendMessagesInOrder(message.channel, alerts);
};

const acknowledgeLater = (found, message) => {
  const alerts = [];

  if (found.works.length) {
    alerts.push(`Success! Added ${found.works.length} work${found.works.length === 1 ? '' : 's'} to your read later list`);
  }

  if (found.missingWorks.length) {
    alerts.push('Works must be in the fandom database to be added. The following URLs were not added to your read laters:');
    found.missingWorks.forEach((url) => {
      alerts.push(url);
    });
  }

  if (found.invalidUrls.length) {
    alerts.push('Please use valid work URLs. The following URLs are not valid AO3 fic URLs:');
    found.invalidUrls.forEach((url) => {
      alerts.push(url);
    });
  }

  sendMessagesInOrder(message.channel, alerts);
};

const adminOnlyMessage = message => message.channel.send('Only admins can perform that action.');
const limitReachedMessage = message => message.channel.send('Woops! Looks like the scrape limit was met. Try requesting a rec from the existing scrapes for now and try again later');

const findPairing = (pairings) => {
  const namedPairings = pairings.map((pair) => {
    if (pair.includes('/')) {
      const [first, second] = pair.split('/');
      const firstName = Object.keys(shortNames)
        .find(name => shortNames[name].includes(first.toLowerCase()));
      const secondName = Object.keys(shortNames)
        .find(name => shortNames[name].includes(second.toLowerCase()));
      return `${firstName || 'blank'}_${secondName || 'blank'}`;
    }
    return pairingNames[pair.toLowerCase()] || 'blank_blank';
  });

  return namedPairings.find(pair => pair !== 'blank_blank') || 'blank_blank';
};

const showFandomsList = (derived, message) => {
  if (derived.notFound) { return null; }
  if (derived.invalid) { return message.delete(); }
  if (!derived.list.length) {
    return message.channel.send('Oops, looks like we couldn\'t find anything for you. Maybe try a different search?');
  }

  const textList = derived.list.reduce((memo, fandom) => {
    const { name, id } = fandom;
    return `${memo}\n**${name}** | \`${id}\``;
  }, '');

  const embed = new Discord.RichEmbed()
    .setTitle('Fandoms List')
    .setFooter(`${derived.range.min + 1} - ${derived.range.max} of ${derived.range.total} | use !next and !prev to navigate list`)
    .setDescription(`
    REMEMBER: You can only get fandom recs by id or url - NOT name
    **<name>** | \`<id>\`
    ${textList}
    `);

  if (derived.existing) {
    return message.channel.fetchMessage(derived.existing)
      .then(listMessage => listMessage.edit(embed))
      .then(() => message.delete());
  }

  return message.channel.send({ embed });
};

const showFavoritesList = (derived, message) => {
  if (derived.notFound) { return null; }
  if (derived.invalid) { return message.delete(); }
  if (!derived.list.length) {
    return message.channel.send('Oops, looks like we couldn\'t find anything for you. Maybe try a different search?');
  }

  const textList = derived.list.reduce((memo, work, i) => {
    const {
      title,
      url,
      authors,
      relationships,
    } = work;

    const [relationship] = relationships.split(',');
    const index = derived.range.min + 1 + i;
    return `${memo}\n**${index}.** [${title}](${url}) by ${authors} - ${relationship}`;
  }, '');

  const embed = new Discord.RichEmbed()
    .setTitle(`${derived.user.username}'s Favorites`)
    .setFooter(`${derived.range.min + 1} - ${derived.range.max} of ${derived.range.total} | use !next and !prev to navigate list`)
    .setDescription(textList);

  if (derived.existing) {
    return message.channel.fetchMessage(derived.existing)
      .then(listMessage => listMessage.edit(embed))
      .then(() => message.delete());
  }

  return message.channel.send({ embed });
};

const showLatersList = (derived, message) => {
  if (derived.notFound) { return null; }
  if (derived.invalid) { return message.delete(); }
  if (!derived.list.length) {
    return message.channel.send('Oops, looks like we couldn\'t find anything for you. Maybe try a different search?');
  }

  const textList = derived.list.reduce((memo, work, i) => {
    const {
      title,
      url,
      authors,
      relationships,
    } = work;

    const [relationship] = relationships.split(',');
    const index = derived.range.min + 1 + i;
    return `${memo}\n**${index}.** [${title}](${url}) by ${authors} - ${relationship}`;
  }, '');

  const embed = new Discord.RichEmbed()
    .setTitle(`${derived.user.username}'s Read Laters`)
    .setFooter(`${derived.range.min + 1} - ${derived.range.max} of ${derived.range.total} | use !next and !prev to navigate list`)
    .setDescription(textList);

  if (derived.existing) {
    return message.channel.fetchMessage(derived.existing)
      .then(listMessage => listMessage.edit(embed))
      .then(() => message.delete());
  }

  return message.channel.send({ embed });
};

const showRandomFandoms = (fandoms, message) => {
  const textList = fandoms.reduce((memo, fandom) => {
    const { name, id } = fandom;
    return `${memo}\n**${name}** | \`${id}\``;
  }, '');

  const embed = new Discord.RichEmbed()
    .setTitle('Fandoms List')
    .setFooter('showing 3 random fandoms')
    .setDescription(`
    REMEMBER: You can only get fandom recs by id or url - NOT name
    **<name>** | \`<id>\`
    ${textList}
    `);

  return message.channel.send({ embed });
};

const showRec = (work, message) => {
  if (!work) { return; }
  if (work.invalidIndex) {
    message.channel.send('Please use a valid index.');
  }
  if (work.empty) {
    message.channel.send('Oops, looks like we couldn\'t find anything for you. Maybe try a different search?');
    return;
  }

  const pairings = work.relationships.split(',').map(t => `__${t}__`).join(', ');
  const otherTags = work.tags.split(',').map(t => `__${t}__`).join(', ');
  const authors = work.authors.split(',').join(', ');

  try {
    const embed = new Discord.RichEmbed()
      .setTitle(work.title)
      .setURL(work.url)
      .setAuthor(authors, TEMP_AVI)
      .setColor(RATING_COLOR[work.rating.trim()])
      .setFooter('Add 👁 to mark this fic as seen | ⭐ - favorite | 🔖 - read later')
      .addField('Rating', work.rating, true)
      .addField('Warnings', work.warnings, true)
      .addField('Chapters', work.chapters, true)
      .addField('Words', work.wordCount, true)
      .addField('Pairing(s)', truncate(pairings, 1024, '__'))
      .addField('Tags', truncate(otherTags, 1024, '__'))
      .addField('Summary', truncate(work.summary, 1024));

    message.channel
      .send({ embed })
      .then((newMessage) => {
        newMessage.react('👁');
        newMessage.react('⭐');
        newMessage.react('🔖');
      });
  } catch (error) {
    if (error.name.includes('RangeError')) {
      message.channel.send('Oops, looks like that fic has a corrupted description. Try again?');
    } else {
      message.channel.send('Oops, looks like something went wrong');
    }
  }
};

const statusTracker = {};

const sendScrapeStatus = (channel, message, fandom, count, deleteMessage = true) => {
  const embed = new Discord.RichEmbed()
    .setTitle(`Scraping ${fandom.name} . . .`)
    .setDescription(`Scraped 0 out of ${count} pages.`);

  return channel.send({ embed })
    .then((mes) => {
      statusTracker[fandom.name] = mes;
      if (deleteMessage) {
        message.delete();
      }
    });
};

const updateScrapeStatus = (fandom, count, finished = false) => {
  const message = statusTracker[fandom.name];
  const description = message.embeds[0].description.replace(/(\b\d+\b){1}/, count);
  const embed = new Discord.RichEmbed({
    ...message.embeds[0],
    description: finished ? `:white_check_mark: ${description}` : description,
  });

  // if (finished) { delete statusTracker[fandom.name]; }
  return message.edit(embed);
};

module.exports = {
  acknowledgeFav,
  acknowledgeLater,
  adminOnlyMessage,
  limitReachedMessage,
  showFandomsList,
  showFavoritesList,
  showLatersList,
  showRandomFandoms,
  showRec,
  sendScrapeStatus,
  updateScrapeStatus,
};

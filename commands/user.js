const User = require('../database/user');
const Option = require('../database/option');

const { FLAGS, INCREMENT } = require('../data/constants');
const { deriveList, getMaxDec } = require('../util');

const getSearchOptions = (searchTerms) => {
  const terms = searchTerms.split(' ');
  const flags = terms.filter(x => x.startsWith('-'));
  const fandoms = terms.filter(x => x && !x.startsWith('-'));

  const ratings = flags
    .filter(f => Object.keys(FLAGS.RATING).includes(f))
    .map(f => FLAGS.RATING[f]);
  const complete = flags.includes('-c');

  return { fandoms, ratings, complete };
};

const getRawFilteredFavorites = (user, searchTerms = '') => {
  const options = getSearchOptions(searchTerms);

  return User.getFavorites(user, options);
};

const getFavoritesFromUserId = (id, searchTerms) => User.findByPk(id)
  .then(user => getRawFilteredFavorites(user, searchTerms));

const getFavoritesFromMessage = savedMes => savedMes.getUser()
  .then(user => getRawFilteredFavorites(user, savedMes.searchTerms));

const getFavoritesSection = savedMes => getFavoritesFromMessage(savedMes)
  .then(favorites => deriveList(favorites, savedMes));

const getFavoritesList = (terms, message, type) => {
  const searchTerms = terms.join(' ');

  if (type === 'new') {
    return User.addOrUpdateMessage(message, {
      position: 0,
      type: 'favorites',
      searchTerms,
    })
      .then(getFavoritesSection);
  }

  return User.getChannelMessage(message)
    .then((saved) => {
      if (!saved) { return null; }

      let position = saved.position + INCREMENT;
      if (type === 'prev') {
        position = getMaxDec(saved.position);
      }

      return saved.update({ position });
    })
    .then(getFavoritesSection);
};

const getRawFilteredLaters = (user, searchTerms = '') => {
  const options = getSearchOptions(searchTerms);

  return User.getLaters(user, options);
};

const getLatersFromUserId = (id, searchTerms) => User.findByPk(id)
  .then(user => getRawFilteredLaters(user, searchTerms));

const getLatersFromMessage = savedMes => savedMes.getUser()
  .then(user => getRawFilteredLaters(user, savedMes.searchTerms));

const getLatersSection = savedMes => getLatersFromMessage(savedMes)
  .then(laters => deriveList(laters, savedMes));

const getLatersList = (terms, message, type) => {
  const searchTerms = terms.join(' ');

  if (type === 'new') {
    return User.addOrUpdateMessage(message, {
      position: 0,
      type: 'laters',
      searchTerms,
    })
      .then(getLatersSection);
  }

  return User.getChannelMessage(message)
    .then((saved) => {
      if (!saved) { return null; }

      let position = saved.position + INCREMENT;
      if (type === 'prev') {
        position = getMaxDec(saved.position);
      }

      return saved.update({ position });
    })
    .then(getLatersSection);
};

const getFicFromList = (message, index) => User.getChannelMessage(message)
  .then((savedMes) => {
    switch (savedMes.type) {
      case 'favorites':
        return getFavoritesFromMessage(savedMes)
          .then(favorites => favorites[index - 1] || { invalidIndex: true });
      case 'laters':
        return getLatersFromMessage(savedMes)
          .then(laters => laters[index - 1] || { invalidIndex: true });
      default:
        return null;
    }
  });

const getOptions = () => Option.findAll();

const isUserAdmin = id => User.findByPk(id)
  .then(user => !!(user && user.admin));

const removeFromList = (message, fic) => User.getChannelMessage(message)
  .then((saved) => {
    if (!saved) { throw new Error('Woops - something went wrong'); }

    return User.findByPk(message.author.id)
      .then((user) => {
        switch (saved.type) {
          case 'favorites':
            return user.removeFavorite(fic)
              .then(() => 'you favorites');
          case 'laters':
            return user.removeLater(fic)
              .then(() => 'you read later list');
          default:
            throw new Error('Woops - something went wrong');
        }
      });
  });

module.exports = {
  getChannelMessage: User.getChannelMessage,
  getFavoritesFromUserId,
  getFavoritesList,
  getLatersFromUserId,
  getLatersList,
  getFicFromList,
  getOptions,
  getSearchOptions: User.getSearchOptions,
  isUserAdmin,
  removeFromList,
  updateMessageId: User.updateMessageId,
};

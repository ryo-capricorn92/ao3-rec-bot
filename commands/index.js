const cli = require('./cli');
const display = require('./display');
const fandom = require('./fandom');
const option = require('./option');
const user = require('./user');
const work = require('./work');

module.exports = {
  cli,
  display,
  fandom,
  option,
  user,
  work,
};

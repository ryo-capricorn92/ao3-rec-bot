const Url = require('url');

const Fandom = require('../database/fandom');
const User = require('../database/user');

const { INCREMENT } = require('../data/constants');
const { deriveList, getMaxDec } = require('../util');

const addOrUpdateFandom = (rawUrl) => {
  const parser = Url.parse(rawUrl);
  const tagValue = parser.pathname.split('/')[2];
  const name = tagValue
    .replace(/\*s\*/gi, '/')
    .replace(/%20/gi, ' ')
    .replace(/\*d\*/gi, '.')
    .replace(/%7C/gi, '|')
    .replace(/%22/gi, '"');
  const url = `${parser.host}${parser.pathname}`;

  const defaults = {
    name,
    url,
  };

  return Fandom
    .findOrCreate({ where: { tagValue }, defaults })
    .spread(fandom => fandom);
};

const getFandomSection = savedMes => Fandom.getAllInOrder()
  .then(fandoms => fandoms.filter(fan => (
    fan.name.toLowerCase().includes(savedMes.searchTerms.toLowerCase())
  )))
  .then(filteredFandoms => deriveList(filteredFandoms, savedMes));

const getFandomsList = (terms, message, type) => {
  const searchTerms = terms.join(' ');

  if (type === 'new') {
    return User.addOrUpdateMessage(message, {
      position: 0,
      type: 'fandoms',
      searchTerms,
    })
      .then(getFandomSection);
  }

  return User.getChannelMessage(message)
    .then((saved) => {
      if (!saved) { return null; }

      let position = saved.position + INCREMENT;
      if (type === 'prev') {
        position = getMaxDec(saved.position);
      }

      return saved.update({ position });
    })
    .then(getFandomSection);
};

const getRandomFandoms = () => Fandom.getRandom(3);
const getAll = () => Fandom.findAll({});

const updateId = (currentId, newId) => Fandom.updateId(currentId, newId);

module.exports = {
  addOrUpdateFandom,
  getAll,
  getFandomsList,
  getRandomFandoms,
  updateId,
};

const { WARNINGS } = require('../data/constants');

const getAuthors = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children('a')
  .filter(function findAuthors() {
    return $(this).attr('rel') === 'author';
  })
  .map(function findAuthorNames() {
    return $(this).text().split('/').slice(-1);
  })
  .get()
  .join(',');

const getChapters = ($, node) => $(node)
  .find('dl.stats')
  .children('.chapters')
  .last()
  .text();

const getLink = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children()
  .first()
  .attr('href');

const getOtherTags = ($, node) => $(node)
  .find('ul.tags')
  .children('li.freeforms')
  .map(function findFreeforms() {
    return $(this).text();
  })
  .get()
  .join(',');

const getPageCount = $ => $('ol.pagination li:not(.next)')
  .last()
  .text();

const getRating = ($, node) => $(node)
  .find('ul.required-tags')
  .children()
  .first()
  .text()
  .trim();

const getRelationshipTags = ($, node) => $(node)
  .find('ul.tags')
  .children('li.relationships')
  .map(function findRelationships() {
    return $(this).text();
  })
  .get()
  .map(t => t.split(' - ')[0])
  .join(',');

const getSummary = ($, node) => $(node)
  .find('blockquote.summary')
  .children('p')
  .map(function findSummaryParts() {
    return $(this).text();
  })
  .get()
  .join(`

  `);

const getTitle = ($, node) => $(node)
  .children()
  .first()
  .children()
  .first()
  .children()
  .first()
  .text();

const getWarnings = ($, node) => $(node)
  .find('ul.tags')
  .children('li.warnings')
  .map(function findWarnings() {
    return WARNINGS[$(this).text()];
  })
  .get()
  .join(',');

const getWordCount = ($, node) => $(node)
  .find('dl.stats')
  .children('.words')
  .last()
  .text();

const getKudos = ($, node) => {
  const kudos = $(node)
    .find('dl.stats')
    .children('.kudos')
    .last()
    .text();
  return parseInt(kudos, 10) || 0;
};

const getBookmarks = ($, node) => {
  const bookmarks = $(node)
    .find('dl.stats')
    .children('.bookmarks')
    .last()
    .text();
  return parseInt(bookmarks, 10) || 0;
};

const calculateScore = ($, node, kudos, bookmarks, medians) => {
  const stats = $(node).find('dl.stats');
  const comments = parseInt(stats.children('.comments').last().text(), 10) || 0;
  const chapters = parseInt(stats.children('.chapters').last().text().split('/')[0], 10) || 1;
  const hits = parseInt(stats.children('.hits').last().text(), 10) || 0;

  const kms = medians.kudos ? (kudos / medians.kudos) * 10 : 0;
  const bms = medians.bookmarks ? (bookmarks / medians.bookmarks) * 10 : 0;

  if (!hits) {
    const bs = (bookmarks / (kudos || 1)) * 100;
    const cs = ((comments / chapters) / (kudos || 1)) * 100;

    return Math.round(bs + cs + kms + bms);
  }

  const ks = (kudos / hits) * 100;
  const bs = (bookmarks / hits) * 1000;
  const cs = ((comments / chapters) / hits) * 1000;

  return Math.round(ks + bs + cs + kms + bms);
};

module.exports = {
  getAuthors,
  getChapters,
  getLink,
  getOtherTags,
  getPageCount,
  getRating,
  getRelationshipTags,
  getSummary,
  getTitle,
  getWarnings,
  getWordCount,
  getKudos,
  getBookmarks,
  calculateScore,
};

const cheerio = require('cheerio');
const request = require('request');

const fromWork = require('../commands/work');

const Work = require('../database/work');

const { getRandomInRange } = require('../util');
const {
  getAuthors,
  getChapters,
  getLink,
  getOtherTags,
  getPageCount,
  getRating,
  getRelationshipTags,
  getSummary,
  getTitle,
  getWarnings,
  getWordCount,
  getKudos,
  getBookmarks,
  calculateScore,
} = require('./meta');

const internals = {};

const findPageCount = url => new Promise((resolve, reject) => {
  if (internals.timeout >= Date.now()) {
    reject();
  } else {
    request(url, (err, response, html) => { // eslint-disable-line consistent-return
      if (err) { return reject(err); }

      const $ = cheerio.load(html);
      const pageCount = getPageCount($);
      resolve(pageCount);
    });
  }
})
  .catch(() => 0);

const getMetaForRandom = async (fandom) => {
  const baseUrl = `https://${fandom.url}`;
  const pageCount = await findPageCount(baseUrl);
  const medians = await fromWork.findMedians(fandom);

  return { pageCount, medians };
};

const scrapeRandomPage = async (fandom, pageCount, medians) => {
  const pageToScrape = getRandomInRange(0, pageCount);
  const url = `https://${fandom.url}?page=${pageToScrape}`;

  return new Promise((resolve, reject) => {
    request(url, (err, response, html) => { // eslint-disable-line consistent-return
      if (err) { return reject(err); }
      if (response.statusCode === 429) {
        const timeoutSeconds = response.headers['retry-after'];
        internals.timeout = Date.now() + (timeoutSeconds * 1000);
        return reject(err);
      }

      const $ = cheerio.load(html);
      const urls = [];

      $('li.work').each(function findAllLinks() {
        const relationships = getRelationshipTags($, this);

        const link = `https://archiveofourown.org${getLink($, this)}`;
        const tags = getOtherTags($, this);
        const summary = getSummary($, this);
        const title = getTitle($, this);
        const chapters = getChapters($, this);
        const wordCount = getWordCount($, this);
        const rating = getRating($, this);
        const warnings = getWarnings($, this);
        const authors = getAuthors($, this);

        const kudos = getKudos($, this);
        const bookmarks = getBookmarks($, this);
        const score = calculateScore($, this, kudos, bookmarks, medians);

        urls.push(link);

        Work.findOrCreate({ where: { url: link } })
          .spread(work => work.update({
            authors,
            bookmarks,
            chapters,
            kudos,
            rating,
            relationships,
            score,
            summary,
            tags,
            title,
            warnings,
            wordCount,
          }))
          .then(((work) => {
            work.addFandom(fandom);
          }));
      });

      resolve(urls);
    });
  })
    .catch((error) => {
      console.error(error); // eslint-disable-line no-console
      return [];
    });
};

const scrapeRandomPages = async (fandom, count) => {
  const { pageCount, medians } = await getMetaForRandom(fandom);

  const recursiveScrape = (rCount = 3, urls = [], tries = 0) => {
    if (internals.timeout >= Date.now()) {
      return { onTimeout: true };
    }

    return scrapeRandomPage(fandom, pageCount, medians)
      .then(batch => [...urls, ...batch])
      .then((newUrls) => {
        if (tries >= rCount) {
          const deduped = Array.from(new Set(newUrls));
          return { urls: deduped };
        }
        return recursiveScrape(rCount, newUrls, tries + 1);
      });
  };

  return recursiveScrape(count);
};

internals.timeout = Date.now();

module.exports = {
  scrapeRandomPage,
  scrapeRandomPages,
};

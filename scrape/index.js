const cheerio = require('cheerio');
const request = require('request');

const Display = require('../commands/display');
const fromWork = require('../commands/work');

const Option = require('../database/option');
const Work = require('../database/work');

const { OPTION_TYPES } = require('../data/constants');

const { scrapeRandomPages } = require('./recs');

const {
  getAuthors,
  getChapters,
  getLink,
  getOtherTags,
  getPageCount,
  getRating,
  getRelationshipTags,
  getSummary,
  getTitle,
  getWarnings,
  getWordCount,
  getKudos,
  getBookmarks,
  calculateScore,
} = require('./meta');

const scrapePage = (url, message, fandom, medians) => {
  request(url, (err, response, html) => { // eslint-disable-line consistent-return
    if (err) { return err; }

    const $ = cheerio.load(html);

    $('li.work').each(function findAllLinks() {
      const relationships = getRelationshipTags($, this);

      const link = `https://archiveofourown.org${getLink($, this)}`;
      const tags = getOtherTags($, this);
      const summary = getSummary($, this);
      const title = getTitle($, this);
      const chapters = getChapters($, this);
      const wordCount = getWordCount($, this);
      const rating = getRating($, this);
      const warnings = getWarnings($, this);
      const authors = getAuthors($, this);

      const kudos = getKudos($, this);
      const bookmarks = getBookmarks($, this);
      const score = calculateScore($, this, kudos, bookmarks, medians);

      Work.findOrCreate({ where: { url: link } })
        .spread(work => work.update({
          authors,
          bookmarks,
          chapters,
          kudos,
          rating,
          relationships,
          score,
          summary,
          tags,
          title,
          warnings,
          wordCount,
        }))
        .then(((work) => {
          work.addFandom(fandom);
        }));
    });
  });
};

// eslint-disable-next-line max-len
const scrapeChainedPages = (url, message, fandom, medians, deleteMessage = true) => new Promise((resolve, reject) => {
  request(url, (err, response, html) => { // eslint-disable-line consistent-return
    if (err) { return reject(err); }
    if (response.statusCode === 429) {
      return reject(err);
    }

    const page = url.split('?page=')[1];
    const $ = cheerio.load(html);

    if (!page) {
      const totalPages = getPageCount($) || '1';
      Option.findOne({ where: { type: OPTION_TYPES.TRACKING_CHANNEL } })
        .then((option) => {
          const channel = message.client.channels.get(option.value);
          Display.sendScrapeStatus(channel, message, fandom, totalPages, deleteMessage)
            .then(() => resolve(html));
        });
    } else if (page.endsWith('0')) {
      Display.updateScrapeStatus(fandom, page)
        .then(() => resolve(html));
    } else {
      resolve(html);
    }
  });
})
  .then((html) => {
    const page = url.split('?page=')[1] || '1';
    const $ = cheerio.load(html);

    $('li.work').each(function findAllLinks() {
      const relationships = getRelationshipTags($, this);

      const link = `https://archiveofourown.org${getLink($, this)}`;
      const tags = getOtherTags($, this);
      const summary = getSummary($, this);
      const title = getTitle($, this);
      const chapters = getChapters($, this);
      const wordCount = getWordCount($, this);
      const rating = getRating($, this);
      const warnings = getWarnings($, this);
      const authors = getAuthors($, this);

      const kudos = getKudos($, this);
      const bookmarks = getBookmarks($, this);
      const score = calculateScore($, this, kudos, bookmarks, medians);

      Work.findOrCreate({ where: { url: link } })
        .spread(work => work.update({
          authors,
          bookmarks,
          chapters,
          kudos,
          rating,
          relationships,
          score,
          summary,
          tags,
          title,
          warnings,
          wordCount,
        }))
        .then(((work) => {
          work.addFandom(fandom);
        }))
        .catch(() => {
          // console.log(link);
          // console.log(error.message);
        });
    });

    const nexts = $('li.next');
    const nextButton = nexts.first();
    if (nexts.get().length && !nextButton.children().first().hasClass('disabled')) {
      const nextUrl = nextButton.children().first().attr('href');
      return scrapeChainedPages(`https://archiveofourown.org${nextUrl}`, message, fandom, medians);
    }
    return Display.updateScrapeStatus(fandom, page, true);
  })
  .catch((err) => {
    console.error(err); // eslint-disable-line no-console
    return Display.limitReachedMessage(message);
  });

const scrapeAllFandoms = (message, fandoms = [], deleteMessage = true) => {
  if (!fandoms.length) return null;
  const [fandom] = fandoms;
  const url = `https://${fandom.url}`;

  return fromWork.findMedians(fandom)
    .then(medians => scrapeChainedPages(url, message, fandom, medians, deleteMessage))
    .then(() => {
      const newFandomsList = fandoms.slice(1);
      if (!newFandomsList.length) { return null; }
      return scrapeAllFandoms(message, newFandomsList, false);
    });
};

module.exports = {
  scrapeAllFandoms,
  scrapeChainedPages,
  scrapePage,
  scrapeRandomPages,
};

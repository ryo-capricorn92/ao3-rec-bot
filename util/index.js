const { INCREMENT } = require('../data/constants');

const getMaxInc = (pos, array) => Math.min(pos + INCREMENT, array.length);
const getMaxDec = pos => Math.max(pos - INCREMENT, 0);

const getPureFicUrl = (url) => {
  if (typeof url !== 'string') { return null; }
  const regex = /^(?:http(?:s)?:\/\/)?archiveofourown.org\/(?:(?:works\/([0-9]+))(?:\/chapters\/[0-9]+)?)?(?:series\/[0-9]+)?(?:#\w+)?/;
  const match = url.match(regex);

  if (!match || !match[1]) { return null; }

  return `https://archiveofourown.org/works/${match[1]}`;
};

const deriveList = (list, savedMes) => {
  if (!savedMes) { return { notFound: true }; }

  if (list.length && savedMes.position + 1 > list.length) {
    return savedMes.update({
      position: getMaxDec(savedMes.position),
    })
      .then(() => ({ invalid: true }));
  }

  const endPosition = getMaxInc(savedMes.position, list);

  return savedMes.getUser()
    .then(user => ({
      existing: savedMes.message,
      list: list.slice(savedMes.position, endPosition),
      range: {
        max: endPosition,
        min: savedMes.position,
        total: list.length,
      },
      user,
    }));
};

const getRandomInRange = (min, max) => Math.floor(Math.random() * ((max - min) + 1)) + min;

const getRandomFromList = (list) => {
  const random = getRandomInRange(0, list.length - 1);
  return list[random];
};

const truncate = (string, max, endTag) => {
  if (max > string.length) { return string; }

  if (endTag) {
    return `${string.slice(0, max - 3 - endTag.length)}...${endTag}`;
  }

  return `${string.slice(0, max - 3)}...`;
};

module.exports = {
  deriveList,
  getMaxDec,
  getMaxInc,
  getPureFicUrl,
  getRandomInRange,
  getRandomFromList,
  truncate,
};

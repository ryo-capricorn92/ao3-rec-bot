const septiplier = [
  'https://archiveofourown.org/tags/Mark%20Fischbach/works',
  'https://archiveofourown.org/tags/Sean%20McLoughlin/works',
  'https://archiveofourown.org/tags/The%20Host/works',
  'https://archiveofourown.org/tags/Bing%20Search%20Engine%20(Anthropomorphic)/works',
];

const allLinks = [
  'https://archiveofourown.org/tags/Sherlock%20Holmes*s*John%20Watson/works', // 2949
  'https://archiveofourown.org/tags/James%20%22Bucky%22%20Barnes*s*Steve%20Rogers/works', // 2088
  'https://archiveofourown.org/tags/Steve%20Rogers*s*Tony%20Stark/works', // 1472
  'https://archiveofourown.org/tags/Will%20Graham*s*Hannibal%20Lecter/works', // 748
  'https://archiveofourown.org/tags/James%20T*d*%20Kirk*s*Spock/works', // 538
  'https://archiveofourown.org/tags/Bakugou%20Katsuki*s*Kirishima%20Eijirou/works', // 426
  'https://archiveofourown.org/tags/Loki*s*Tony%20Stark/works', // 414
  'https://archiveofourown.org/tags/Peter%20Parker*s*Wade%20Wilson/works', // 297
  'https://archiveofourown.org/tags/Loki*s*Thor%20(Marvel)/works', // 292
  'https://archiveofourown.org/tags/Mark%20Fischbach*s*Sean%20McLoughlin/works', // 206
  'https://archiveofourown.org/tags/Hank%20Anderson*s*Connor/works', // 176
  'https://archiveofourown.org/tags/Eddie%20Brock*s*Venom%20Symbiote/works', // 125
  'https://archiveofourown.org/tags/Upgraded%20Connor%20%7C%20RK900*s*Gavin%20Reed/works', // 110
  'https://archiveofourown.org/tags/Axel*s*Roxas%20(Kingdom%20Hearts)/works', // 82
  'https://archiveofourown.org/tags/Aizawa%20Shouta%20%7C%20Eraserhead*s*Yagi%20Toshinori%20%7C%20All%20Might/works', // 65
  'https://archiveofourown.org/tags/Rick%20Sanchez*s*Morty%20Smith/works', // 57
  'https://archiveofourown.org/tags/Jack%20Frost*s*Pitch%20Black/works', // 48
  'https://archiveofourown.org/tags/Mark%20Fischbach*s*Mark%20Fischbach/works', // 25
  'https://archiveofourown.org/tags/Sean%20McLoughlin*s*Sean%20McLoughlin/works', // 15
  'https://archiveofourown.org/tags/Sean%20McLoughlin*s*Robin%20Torkar/works', // 2
];

module.exports = {
  allLinks,
  septiplier,
};

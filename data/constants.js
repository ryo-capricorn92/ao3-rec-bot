const AVATARS = {
  blank: 'https://cdn.discordapp.com/attachments/437046889846931457/465623526435586052/blank.png',
  test: 'https://i.imgur.com/duGWUvh.png',
};

const FLAGS = {
  RATING: {
    '-g': 'General Audiences',
    '-t': 'Teen And Up Audiences',
    '-m': 'Mature',
    '-e': 'Explicit',
    '-nr': 'Not Rated',
  },
  OPTIONS: {
    COMPLETE: '-c',
    FRESH: '-f',
  },
};

const HELP = {
  DESCRIPTION: `
All recommendations made by this bot are based off a custom algorithm determining "good" fics by ratio of kudos/bookmarks/comments to hits rather than strictly by quantity. The algorithm is still in development, and will be improved with time and testing.
⠀
  `,
  START: `
Try using the command \`!rec\` to get a random fic recommendation!

You can see options below for getting more specific recs.
  `,
  FANDOMS: `
"Fandoms" are how fics are organized. They are usually a specific pairing, but can be just a particular tag on AO3.

\`!fandoms\`
⠀⠀List all available fandoms
\`!fandoms Spock\`
⠀⠀List all fandoms that contain "Spock"

A fandom in the fandom list will look like this:
⠀⠀**Fandom Name** \`shortname\`
eg, **Will Graham/Hannibal Lecter** \`hannigram\`

Fandom "shortnames" must be used when getting recs.

  `,
  RECS: `
"Recs" are fic recommendations made by the bot. \`!random\` and \`!rec\` can be used interchangeably with all search options.

\`!random\`
⠀⠀Get a random fic recommendation
\`!rec\`
⠀⠀Get a random fic the user has not marked as "seen"
\`!rec hannigram\`
⠀⠀Get a random hannigram rec
\`!rec hannigram spirk\`
⠀⠀Get a random rec that's either hannigram or spirk
\`!rep\`
⠀⠀Repeat the last search performed by this user
⠀
  `,
  FLAGS: `
"Flags" can be added to searches to narrow down possible recs. Multiple flags can be used.

\`-g\` - "General Audiences" rating
\`-t\` - "Teen and Up" rating
\`-m\` - "Mature" rating
\`-e\` - "Explicit" rating
\`-nr\` - "Not Rated" rating
\`-c\` - Completed works only (no WIPs)

\`!rec stucky stony -g -t -c\`
⠀⠀Will find a rec that is either stucky or stony, either "General Audiences" or "Teen and Up", and is completed
⠀
  `,
  REACTS: `
"Reactions" are emojis a user can add to a fic recommendation to "mark" that fic into a personal category. Users can add reacts to any rec, not just their own.

:eye: - Will mark a fic as "seen". Seen fics will not show up in \`!rec\` recommendations

:star: - Will mark a fic as a favorite.

:bookmark: - Will mark a fic to "read later".
  `,
  FAVS: `
"Favorites" are a list of user-saved fics specific to each user.

\`!favs\`
⠀⠀Show list of user's saved favorites
\`!fav\`
⠀⠀Get a random fic from user's favorites

\`!fav\` can be used with the same search options as \`!rec\` and \`!random\`
⠀
  `,
  LATERS: `
"Read Laters" behave identically to Favorites, but are meant to be used for marking fics for later reading.

\`!laters\`
⠀⠀Show list of user's saved read laters
\`!later\`
⠀⠀Get a random fic from user's read laters

\`!later\` can be used with the same search options as \`!rec\` and \`!random\`
⠀
  `,
};

const INCREMENT = 10;

const OPTION_TYPES = {
  TRACKING_CHANNEL: 'tracking_channel',
};

const RATING_COLOR = {
  'General Audiences': '#8FC426',
  'Teen And Up Audiences': '#F1DF3F',
  Mature: '#EF8932',
  Explicit: '#A70A12',
};

const TEMP_AVI = 'http://i.imgur.com/b52FKPE.png';
const TEMP_THUMBNAIL = 'http://i.imgur.com/p2qNFag.png';

const WARNINGS = {
  'Creator Chose Not To Use Archive Warnings': '???',
  'No Archive Warnings Apply': 'None',
  'Graphic Depictions Of Violence': 'Violence',
  'Rape/Non-Con': 'Rape',
  'Major Character Death': 'Death',
  Underage: 'Underage',
};

module.exports = {
  AVATARS,
  FLAGS,
  HELP,
  INCREMENT,
  OPTION_TYPES,
  RATING_COLOR,
  TEMP_AVI,
  TEMP_THUMBNAIL,
  WARNINGS,
};

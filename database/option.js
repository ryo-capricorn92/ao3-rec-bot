const Sequelize = require('sequelize');
const db = require('./db');

const Option = db.define('option', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true,
  },
  server: Sequelize.STRING,
  type: Sequelize.STRING,
  value: {
    type: Sequelize.STRING,
    get() {
      const value = this.getDataValue('value');
      if (value === 'true') { return true; }
      if (value === 'false') { return false; }
      if (value === 'undefined') { return undefined; }
      if (value === 'null') { return null; }
      return value;
    },
    set(val) {
      this.setDataValue('value', `${val}`);
    },
  },
  adminOnly: Sequelize.BOOLEAN,
});

Option.addOrUpdateOption = newOption => Option.findOrCreate({ where: { type: newOption.type } })
  .spread(option => option.update({
    server: newOption.server || '',
    value: newOption.value || '',
    adminOnly: newOption.adminOnly || false,
  }));

module.exports = Option;

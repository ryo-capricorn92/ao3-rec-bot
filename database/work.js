const Sequelize = require('sequelize');
const db = require('./db');

const Fandom = require('./fandom');
const User = require('./user');

const Work = db.define('work', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
  },
  authors: Sequelize.STRING,
  bookmarks: Sequelize.INTEGER,
  chapters: Sequelize.STRING,
  kudos: Sequelize.INTEGER,
  rating: Sequelize.STRING,
  relationships: Sequelize.TEXT,
  score: Sequelize.INTEGER,
  summary: Sequelize.TEXT,
  tags: Sequelize.TEXT,
  title: Sequelize.STRING,
  url: {
    type: Sequelize.STRING,
    primaryKey: true,
  },
  warnings: Sequelize.STRING,
  wordCount: Sequelize.STRING,
});

Work.belongsToMany(Fandom, { through: 'fandom_work' });
Fandom.belongsToMany(Work, { through: 'fandom_work' });

// Favorites
Work.belongsToMany(User, {
  as: 'favorite',
  through: 'favorites',
  foreignKey: 'workId',
  otherKey: 'userId',
});
User.belongsToMany(Work, {
  as: 'favorite',
  through: 'favorites',
  foreignKey: 'userId',
  otherKey: 'workId',
});

// Read Laters
Work.belongsToMany(User, {
  as: 'later',
  through: 'read_laters',
  foreignKey: 'workId',
  otherKey: 'userId',
});
User.belongsToMany(Work, {
  as: 'later',
  through: 'read_laters',
  foreignKey: 'userId',
  otherKey: 'workId',
});

// Read
Work.belongsToMany(User, {
  as: 'read',
  through: 'reads',
  foreignKey: 'workId',
  otherKey: 'userId',
});
User.belongsToMany(Work, {
  as: 'read',
  through: 'reads',
  foreignKey: 'userId',
  otherKey: 'workId',
});

Work.findByUrl = url => Work.findByPk(url);

Work.getRandom = (options, urls) => {
  const include = [];
  const where = {};

  if (urls && urls.length) {
    where.id = {
      [Sequelize.Op.in]: urls,
    };
  }

  if (options.complete) {
    where.chapters = {
      [Sequelize.Op.regexp]: '(\\d+)\\/+\\1',
    };
  }

  if (options.ratings.length) {
    where.rating = {
      [Sequelize.Op.in]: options.ratings,
    };
  }

  if (options.fandoms.length) {
    include.push({
      model: Fandom,
      where: {
        id: {
          [Sequelize.Op.in]: options.fandoms,
        },
      },
    });
  }

  return Work.findAll({
    limit: 50,
    order: Sequelize.literal('rand()'), // mysql
    // order: Sequelize.literal('random()'), // postgres
    include,
  });
};

Work.getRandomUnreadByUser = (userId, options, urls) => {
  const include = [{
    model: User,
    as: 'read',
    required: false,
    attributes: ['id'],
    where: { id: userId },
  }];

  const where = {
    '$read.id$': null,
  };

  if (urls && urls.length) {
    if (options.limit) {
      where.id = {
        [Sequelize.Op.notIn]: urls,
      };
    } else {
      where.id = {
        [Sequelize.Op.in]: urls,
      };
    }
  }

  if (options.complete) {
    where.chapters = {
      [Sequelize.Op.regexp]: '(\\d+)\\/+\\1',
    };
  }

  if (options.ratings.length) {
    where.rating = {
      [Sequelize.Op.in]: options.ratings,
    };
  }

  if (options.fandoms.length) {
    include.push({
      model: Fandom,
      where: {
        id: {
          [Sequelize.Op.in]: options.fandoms,
        },
      },
    });
  }

  return Work.findAll({
    limit: options.limit || 50,
    order: Sequelize.literal('rand()'), // mysql
    // order: Sequelize.literal('random()'), // postgres
    subQuery: false,
    include,
    where,
  });
};

Work.getTopByField = (fandom, field, limit = 20) => Work.findAll({
  limit,
  order: [[field, 'DESC']],
  include: [{
    model: Fandom,
    where: {
      id: fandom,
    },
  }],
});

module.exports = Work;

const Sequelize = require('sequelize');
const shortid = require('shortid');
const db = require('./db');

const Fandom = db.define('fandom', {
  id: {
    type: Sequelize.STRING,
    defaultValue() {
      let id;
      do {
        id = shortid.generate();
      } while (id.includes('_') || id.includes('-'));
      return id;
    },
    primaryKey: true,
  },
  name: Sequelize.STRING,
  tagValue: Sequelize.STRING,
  url: Sequelize.STRING,
});

Fandom.getFandomsFromLists = ({ ids, urls }) => Fandom.findAll({
  where: {
    [Sequelize.Op.or]: [
      {
        id: {
          [Sequelize.Op.in]: ids,
        },
      },
      {
        url: {
          [Sequelize.Op.in]: urls,
        },
      },
    ],
  },
});

Fandom.getAllInOrder = () => Fandom.findAll({
  order: [['name', 'ASC']],
});

Fandom.getRandom = count => Fandom.findAll({
  limit: count,
  order: Sequelize.literal('rand()'), // mysql
  // order: Sequelize.literal('random()'), // postgres
});

Fandom.updateId = (currentId, newId) => Fandom.update({
  id: newId,
}, {
  where: {
    id: currentId,
  },
});

module.exports = Fandom;

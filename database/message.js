const Sequelize = require('sequelize');
const db = require('./db');

const Message = db.define('message', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true,
  },
  channel: Sequelize.STRING,
  message: Sequelize.STRING,
  position: Sequelize.INTEGER,
  searchTerms: Sequelize.STRING,
  type: Sequelize.STRING,
});

module.exports = Message;

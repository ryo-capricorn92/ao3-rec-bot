const Sequelize = require('sequelize');
const db = require('./db');

const Message = require('./message');
const Fandom = require('./fandom');

const User = db.define('user', {
  id: {
    type: Sequelize.STRING,
    primaryKey: true,
  },
  fandoms: Sequelize.STRING,
  ratings: Sequelize.STRING,
  type: Sequelize.STRING,
  username: Sequelize.STRING,
  admin: Sequelize.BOOLEAN,
});

User.hasMany(Message);
Message.belongsTo(User);

User.getSearchOptions = ({ id, username }) => {
  const defaults = {
    fandoms: '',
    ratings: '',
    type: '',
    username,
  };

  return User.findOrCreate({ where: { id }, defaults }).spread(user => ({
    fandoms: user.fandoms.split(','),
    flags: user.ratings.split(','),
    type: user.type,
  }));
};

User.setOptions = ({ id, username }, { fandoms, flags, type }) => {
  const defaults = {
    fandoms: '',
    ratings: '',
    type: '',
    username,
  };

  return User.findOrCreate({ where: { id }, defaults }).spread(user => user.update({
    fandoms: fandoms.join(','),
    ratings: flags.join(','),
    type,
  }));
};

User.addOrUpdateMessage = ({ author, channel }, { position, searchTerms = '', type }) => {
  const defaults = {
    fandoms: '',
    ratings: '',
    type: '',
    username: author.username,
  };

  return User.findOrCreate({ where: { id: author.id }, defaults })
    .spread(user => user.getMessages()
      .then((messages) => {
        const message = messages.find(m => m.channel === channel.id);

        if (message) {
          return message.update({
            message: '',
            position,
            searchTerms,
            type,
          });
        }

        return Message.create({
          channel: channel.id,
          message: '',
          position,
          searchTerms,
          type,
        }).then(newMessage => user.addMessage(newMessage).then(() => newMessage));
      }));
};

User.getChannelMessage = ({ author, channel }) => User.findByPk(author.id)
  .then((user) => {
    if (!user) { return null; }
    return user.getMessages()
      .then(messages => messages.find(m => m.channel === channel.id));
  });

User.updateMessageId = (message, id) => User.getChannelMessage(message)
  .then(saved => saved.update({
    message: id,
  }));

User.getFavorites = (user, { fandoms, complete, ratings }) => {
  const options = {};

  if (complete) {
    if (!options.where) {
      options.where = {};
    }
    options.where.chapters = {
      [Sequelize.Op.regexp]: '(\\d+)\\/+\\1',
    };
  }

  if (ratings && ratings.length) {
    if (!options.where) {
      options.where = {};
    }
    options.where.rating = {
      [Sequelize.Op.in]: ratings,
    };
  }

  if (fandoms && fandoms.length) {
    options.include = [{
      model: Fandom,
      where: {
        id: {
          [Sequelize.Op.in]: fandoms,
        },
      },
    }];
  }

  return user.getFavorite(options);
};

User.getLaters = (user, { fandoms, complete, ratings }) => {
  const options = {};

  if (complete) {
    if (!options.where) {
      options.where = {};
    }
    options.where.chapters = {
      [Sequelize.Op.regexp]: '(\\d+)\\/+\\1',
    };
  }

  if (ratings && ratings.length) {
    if (!options.where) {
      options.where = {};
    }
    options.where.rating = {
      [Sequelize.Op.in]: ratings,
    };
  }

  if (fandoms && fandoms.length) {
    options.include = [{
      model: Fandom,
      where: {
        id: {
          [Sequelize.Op.in]: fandoms,
        },
      },
    }];
  }

  return user.getLater(options);
};

module.exports = User;
